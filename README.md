## openvpn-genmon-linux using surfshark vpn ::: will work with many providers.

``GENMONITOR`` xfce4 script for the panel. Checks if a vpn is running or not. With IP tooltip.

`$ check-vpn-gen  - uses script speedtest-notif.sh` <speedtest.net>

## openvpn-connect folder :

Bash scripts to setup auto launch vpn connection - usefull for puppylinux. HowTo in folder.

### DEPS 

speedtest-cli

openvpn

notify-send

zenity

ipgeo [Github archived : linux :](https://github.com/jakewmeyer/Geo) : arch-aur :

netstat

ifconfig

hwinfo

### LOCATION : check-vpn tray  system wide . . . 

/usr/local/bin/check-vpn-gen

/usr/local/bin/speedtest-notif.sh

~/.icons

---

### NOTIFICATION

![Screenshot_2022-01-19_12-36-18](/uploads/659327fee20574d09e704b223d55cae6/Screenshot_2022-01-19_12-36-18.png)

![Schermafdruk_2021-11-26_16-08-24](/uploads/8025f42c334e0f4bd83ddda5672006d8/Schermafdruk_2021-11-26_16-08-24.png)

![Peek_2021-11-26_16-28](/uploads/9d0d04c6003c6c5f0a2b6aec2b6421fb/Peek_2021-11-26_16-28.gif)
